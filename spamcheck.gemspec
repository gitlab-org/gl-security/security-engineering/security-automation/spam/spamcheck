# coding: utf-8
prefix = 'ruby'
$LOAD_PATH.unshift(File.expand_path(File.join(prefix), __dir__))
require 'spamcheck/version'

Gem::Specification.new do |spec|
  spec.name          = "spamcheck"
  spec.version       = Spamcheck::VERSION
  spec.authors       = ["GitLab Security Automation"]
  spec.email         = ["security-automation@gitlab.com"]

  spec.summary       = %q{Auto-generated gRPC client for Spamcheck}
  spec.description   = %q{Auto-generated gRPC client for Spamcheck.}
  spec.homepage      = "https://gitlab.com/gitlab-org/gl-security/security-engineering/security-automation/spam/spamcheck"
  spec.license       = "MIT"

  spec.files         = `find ./#{prefix}`.split("\n").reject { |f| f.match(%r{^#{prefix}/(test|spec|features)/}) }
  spec.require_paths = [prefix]

  spec.add_dependency "grpc", "~> 1.63"
end
