import argparse
import os
from unittest import TestCase, mock

from app.config import Config


class TestConfig(TestCase):
    def test_defaults(self):
        config = Config()
        config.load()

        self.assertEqual(
            "info", config.get("log_level"), "Default log level should be INFO"
        )
        self.assertEqual(
            "development", config.get("env"), "Default env should be development"
        )
        self.assertEqual(
            "0.0.0.0:8001",
            config.get("grpc_addr"),
            "Default grpc_addr should be 0.0.0.0:8001",
        )
        self.assertEqual(
            "spamcheck",
            config.get("google_pubsub_topic"),
            "Default google_pubsub_topic should be spamcheck",
        )
        self.assertEqual(
            "./ssl/cert.pem",
            config.get("tls_certificate"),
            "Default tls_certificate should be ./ssl/cert.pem",
        )
        self.assertEqual(
            "./ssl/key.pem",
            config.get("tls_private_key"),
            "Default tls_private_key should be ./ssl/key.pem",
        )
        self.assertEqual(
            {},
            config.get("filter.allow_list"),
            "Default filter.allow_list should be empty",
        )
        self.assertEqual(
            {},
            config.get("filter.deny_list"),
            "Default filter.deny_list should be empty",
        )
        self.assertEqual(
            [],
            config.get("filter.allowed_domains"),
            "Default filter.allowed_domains should be empty",
        )

        self.assertEqual(
            "ALLOW",
            config.get("generic.max_verdict"),
            "Generic max verdict should be ALLOW",
        )
        self.assertEqual(
            "ALLOW", config.get("note.max_verdict"), "Note max verdict should be ALLOW"
        )
        self.assertEqual(
            "CONDITIONAL_ALLOW",
            config.get("issue.max_verdict"),
            "Issue max verdict should be CONDITIONAL_ALLOW",
        )
        self.assertEqual(
            "CONDITIONAL_ALLOW",
            config.get("snippet.max_verdict"),
            "Snippet max verdict should be CONDITIONAL_ALLOW",
        )

        for spammable in ["issue", "snippet", "note", "generic"]:
            self.assertEqual(
                config.DEFAULT_BLOCK_THRESHOLD,
                config.get(f"{spammable}.block_threshold"),
                f"Default block threshold for {spammable} should match",
            )
            self.assertEqual(
                config.DEFAULT_DISALLOW_THRESHOLD,
                config.get(f"{spammable}.disallow_threshold"),
                f"Default block threshold for {spammable} should match",
            )
            self.assertEqual(
                config.DEFAULT_CONDITIONAL_ALLOW_THRESHOLD,
                config.get(f"{spammable}.conditional_allow_threshold"),
                f"Default block threshold for {spammable} should match",
            )

    @mock.patch.dict(
        os.environ,
        {
            "SPAMCHECK_ISSUE_MAX_VERDICT": "BLOCK",
            "SPAMCHECK_ISSUE_BLOCK_THRESHOLD": "0.7",
        },
    )
    def test_load_from_env(self):
        config = Config()
        config.load()

        self.assertEqual(
            0.7,
            config.get_float("issue.block_threshold"),
            "Block threshold for issue should match env var",
        )

        self.assertEqual(
            "BLOCK",
            config.get_string("issue.max_verdict"),
            "Max verdict for issue should match env var",
        )

    @mock.patch("argparse.ArgumentParser.parse_known_args")
    def test_load_cli_args(self, mock_args):
        args = {
            "config": None,
            "issue.max_verdict": "BLOCK",
            "issue.block_threshold": 0.6,
        }
        mock_args.return_value = (argparse.Namespace(**args), [])

        config = Config()
        config.load()

        self.assertEqual(
            0.6,
            config.get_float("issue.block_threshold"),
            "Block threshold for issue should match CLI arg",
        )

        self.assertEqual(
            "BLOCK",
            config.get_string("issue.max_verdict"),
            "Max verdict for issue should match CLI arg",
        )

    def test_load_file(self):
        config = Config()
        config.set("config", "./tests/fixtures/config.yml")
        config.load()

        self.assertEqual(
            0.75,
            config.get_float("issue.block_threshold"),
            "Block threshold for issue should match env var",
        )

        self.assertEqual(
            "BLOCK",
            config.get_string("issue.max_verdict"),
            "Max verdict for issue should match config file",
        )

        self.assertEqual(
            0.6,
            config.get_float("generic.conditional_allow_threshold"),
            "Block threshold for generic should match config file",
        )

        self.assertEqual(
            "CONDITIONAL_ALLOW",
            config.get_string("generic.max_verdict"),
            "Max verdict for issue should match config file",
        )

        self.assertEqual(
            0.8,
            config.get_float("snippet.disallow_threshold"),
            "Block threshold for snippet should match config file",
        )

        self.assertEqual(
            "DISALLOW",
            config.get_string("snippet.max_verdict"),
            "Max verdict for snippet should match config file",
        )

        self.assertEqual(
            ["gitlab.com"],
            config.get("filter.allowed_domains"),
            "Allowed domains should match config file",
        )

        self.assertEqual(
            {1234: "spamtest/hello"},
            config.get("filter.allow_list"),
            "Allow list should match config file",
        )

        self.assertEqual(
            "127.0.0.1:8001",
            config.get_string("grpc_addr"),
            "Allow list should match config file",
        )

        self.assertTrue(
            config.get_bool("tls_enabled"),
            "When key and cert files exist TLS should be enabled",
        )

    def test_load_file_from_default_location(self):
        config = Config()
        config.add_config_path("./tests/fixtures")
        config.load()

        self.assertEqual(
            "127.0.0.1:8001",
            config.get_string("grpc_addr"),
            "Allow list should match config file",
        )

    @mock.patch("argparse.ArgumentParser.parse_known_args")
    def test_load_file_from_cli_arg(self, mock_args):
        args = {"config": "./tests/fixtures/config.yml"}
        mock_args.return_value = (argparse.Namespace(**args), [])

        config = Config()
        config.load()

        self.assertEqual(
            "127.0.0.1:8001",
            config.get_string("grpc_addr"),
            "Allow list should match config file",
        )

    def test_get(self):
        config = Config()
        config.set("test", "aaa")

        self.assertEqual(
            11,
            config.get("nonexistent", 11),
            "Missing config value should return default",
        )

        self.assertEqual(
            "aaa",
            config.get("test", "abc"),
            "Found config key should return stored value",
        )

    def test_get_string(self):
        config = Config()
        config.set("test", "aaa")

        self.assertEqual(
            "11",
            config.get_string("nonexistent", 11),
            "Missing config value should return default as string",
        )

        self.assertEqual(
            "aaa",
            config.get_string("test", "abc"),
            "Found config key should return stored value",
        )

    def test_get_float(self):
        config = Config()
        config.set("test", 1.0)

        self.assertEqual(
            11.0,
            config.get_float("nonexistent", 11),
            "Missing config value should return default as float",
        )

        self.assertEqual(
            1.0,
            config.get_float("test", 2.0),
            "Found config key should return stored value",
        )

    def test_read_in_config(self):
        config = Config()

        with mock.patch("app.config.log.error") as mock_log:
            config_file = "nonexistent.yml"
            config.set("config", config_file)
            config.load()
            mock_log.assert_called_once_with(
                "Config file not found", extra={"file_name": config_file}
            )

        with mock.patch("app.config.log.error") as mock_log:
            config_file = os.path.abspath("./tests/fixtures/invalid_config.yml")
            config.set("config", config_file)
            config.load()
            mock_log.assert_called_once_with(
                "Failed to read config file", extra={"file_name": config_file}
            )

        with mock.patch("app.config.gcs.watch_file") as mock_gcs:
            config_file = "gs://fake-bucket/config.yml"
            config.set("config", config_file)

            with mock.patch("os.path.exists") as mock_exists:
                mock_exists.return_value = True
                config.load()
                mock_gcs.assert_called_once_with(config_file, ".gcs/config.yml")

            # Recheck 5 times if gcs file is not present locally
            with mock.patch("os.path.exists") as mock_exists:
                mock_exists.return_value = False
                with mock.patch("app.config.time") as mock_time:
                    config.load()
                    self.assertEqual(5, mock_time.sleep.call_count)

    def test_callbacks(self):
        config = Config()
        config.load()

        callback = mock.Mock()
        bad_callback = mock.Mock()
        bad_callback.side_effect = Exception("Boom!")

        config.register_callback(callback)
        config.register_callback(bad_callback)

        with mock.patch("app.config.log.error") as mock_log:
            config._on_config_change()
            callback.assert_called_once()
            bad_callback.assert_called_once()
            mock_log.assert_called_once_with("failed to run config callback")
