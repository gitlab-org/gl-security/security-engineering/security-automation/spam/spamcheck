import unittest
from unittest.mock import patch

from app.config import Config
from app.deny_list import DenyList, config


class TestDenyList(unittest.TestCase):

    def test_has_domain(self):
        dl = DenyList("fake-key", "fake-key")

        dl.data = {"example.com": 0.5}

        self.assertTrue(dl.has_domain("example.com", 0.6), "Deny list should have domain")
        self.assertFalse(dl.has_domain("example.com", 0.4), "Deny list domain does not meet threshold")
        self.assertFalse(dl.has_domain("foo.example.com", 0.6), "Deny list should not have domain")

        dl.data = {"*.example.com": 0.5}

        self.assertFalse(dl.has_domain("example.com", 0.6), "Deny list should not have domain")
        self.assertFalse(dl.has_domain("example.com", 0.4), "Deny list domain does not meet threshold")
        self.assertTrue(dl.has_domain("foo.example.com", 0.9), "Deny list should have domain")

    def test_load_deny_list(self):
        config = Config()
        config.set("filepath", "tests/fixtures/deny_list.txt")
        config.set("threshold", 0.5)

        with patch('app.deny_list.config', config):
            dl = DenyList("filepath", "threshold")
            dl._load_deny_list(dl._filename)

            self.assertEqual(0.4, dl.data["foo.com"], "Correct threshold should be set for domain")
            self.assertEqual(0.9, dl.data["bar.com"], "Correct threshold should be set for domain")
            self.assertEqual(0.5, dl.data["baz.com"], "Correct threshold should be set for domain")
