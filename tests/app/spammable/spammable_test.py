import unittest
from datetime import datetime
from unittest.mock import patch

import api.v1.spamcheck_pb2 as spam
from google.protobuf.timestamp_pb2 import Timestamp

from app.config import Config
from app.deny_list import DenyList
from app.spammable import Spammable
from app.spammable.issue import Issue
from tests.app.helpers import MockContext


class TestSpam(unittest.TestCase):
    mock_issue = {
        "title": "mock spam",
        "description": "mock spam https://livestream.com https://subdomain.hd-streaming.com",
        "user_in_project": True,
        "project": {
            "project_id": 555,
            "project_path": "mock/spmmable",
        },
        "user": {
            "emails": [{"email": "test@example.com", "verified": True}],
            "username": "test",
            "org": "test",
            "id": 23,
            "abuse_metadata": {
                "account_age": 3,
                "spam_score": 0.32,
            },
        },
    }

    def test_project_allowed(self):
        mock_project = {123: "spamtest/hello"}
        s = Issue(spam.Issue(**self.mock_issue), None)

        allowed = s.project_allowed(555)
        self.assertTrue(allowed, "Empty allow and deny list should return True")

        s.allow_list = mock_project
        allowed = s.project_allowed(555)
        self.assertFalse(allowed, "Project not in allow_list should return False")

        allowed = s.project_allowed(123)
        self.assertTrue(allowed, "Project in allow_list should return True")

        s.allow_list = {}
        s.deny_list = mock_project
        allowed = s.project_allowed(555)
        self.assertTrue(allowed, "Project not in deny_list should return True")

        allowed = s.project_allowed(123)
        self.assertFalse(allowed, "Project in deny_list should return False")

    def test_email_allowed(self):
        def set_emails(email, verified):
            args = {"user": {"emails": [{"email": email, "verified": verified}]}}
            i = spam.Issue(**args)
            return i.user.emails

        s = Issue(spam.Issue(**self.mock_issue), None)
        Spammable.allowed_domains = {"gitlab.com"}
        emails = set_emails("integrationtest@example.com", True)
        allowed = s.email_allowed(emails)
        self.assertFalse(allowed, "Non gitlab email should not be allowed")

        emails = set_emails("integrationtest@gitlab.com", True)
        allowed = s.email_allowed(emails)
        self.assertTrue(allowed, "Verified gitlab email should be allowed")

        emails = set_emails("integrationtest@gitlab.com", False)
        allowed = s.email_allowed(emails)
        self.assertFalse(allowed, "Non-verified gitlab email should not be allowed")

    def test_user_has_id(self):
        s = spam.Issue(**self.mock_issue)
        self.assertEqual(23, s.user.id, "User ID does not match")

    def test_abuse_metadata(self):
        s = spam.Issue(**self.mock_issue)
        error_msg = "Abuse metadata has unepected value"

        self.assertEqual(3, s.user.abuse_metadata["account_age"], error_msg)
        self.assertAlmostEqual(0.32, s.user.abuse_metadata["spam_score"], msg=error_msg)

    def test_to_dict(self):
        spammable = Issue(spam.Issue(**self.mock_issue), MockContext()).to_dict()
        error_msg = "Expected field not seen in spammable dictionary"

        self.assertTrue("user" in spammable, error_msg)
        self.assertTrue("title" in spammable, error_msg)
        self.assertTrue("description" in spammable, error_msg)
        self.assertTrue("project" in spammable, error_msg)
        self.assertTrue("userInProject" in spammable, error_msg)

        user = spammable["user"]

        self.assertTrue("username" in user, error_msg)
        self.assertTrue("id" in user, error_msg)
        self.assertTrue("abuseMetadata" in user, error_msg)
        self.assertTrue("emails" in user, error_msg)
        self.assertTrue("org" in user, error_msg)

    def test_max_verdict(self):
        config = Config()
        config.load()

        with patch("app.spammable.config", config):
            self.assertEqual(
                spam.SpamVerdict.CONDITIONAL_ALLOW,
                Issue.max_verdict,
                "Unexpected maximum verdict for Issue",
            )

            config.set("issue.max_verdict", "ALLOW")
            Issue.set_max_verdict()
            self.assertEqual(
                spam.SpamVerdict.ALLOW,
                Issue.max_verdict,
                "Unexpected maximum verdict for Issue",
            )

    def test_calculate_verdict(self):
        s = Issue(spam.Issue(**self.mock_issue), None)

        verdict, reason = s.calculate_verdict(1.0)
        self.assertEqual(
            spam.SpamVerdict.CONDITIONAL_ALLOW, verdict, "Unexpected verdict"
        )
        self.assertEqual(
            "ml inference score overriden by max verdict",
            reason,
            "Unexpected verdict reason",
        )

        deny_list = DenyList("file-key", "threshold-key")

        deny_list.data = {"livestream.com": 0.5}
        with patch("app.spammable.domain_disallow_list", deny_list):
            verdict, reason = s.calculate_verdict(1.0)
            self.assertEqual(
                spam.SpamVerdict.DISALLOW,
                verdict,
                "Verdict for domain in disallow list should be DISALLOW",
            )
            self.assertEqual(
                "ml inference score overriden by domain disallow list",
                reason,
                "Unexpected verdict reason",
            )

        with patch("app.spammable.domain_block_list", deny_list):
            verdict, reason = s.calculate_verdict(1.0)
            self.assertEqual(
                spam.SpamVerdict.BLOCK,
                verdict,
                "Verdict for domain in block list should be BLOCK",
            )
            self.assertEqual(
                "ml inference score overriden by domain block list",
                reason,
                "Unexpected verdict reason",
            )

        deny_list.data = {"*.hd-streaming.com": 0.5}
        with patch("app.spammable.domain_disallow_list", deny_list):
            verdict, reason = s.calculate_verdict(1.0)
            self.assertEqual(
                spam.SpamVerdict.DISALLOW,
                verdict,
                "Verdict for wildcard domain in disallow list should be DISALLOW",
            )
            self.assertEqual(
                "ml inference score overriden by domain disallow list",
                reason,
                "Unexpected verdict reason",
            )

        with patch("app.spammable.domain_block_list", deny_list):
            verdict, reason = s.calculate_verdict(1.0)
            self.assertEqual(
                spam.SpamVerdict.BLOCK,
                verdict,
                "Verdict for wildcard domain in block list should be BLOCK",
            )
            self.assertEqual(
                "ml inference score overriden by domain block list",
                reason,
                "Unexpected verdict reason",
            )

        # When verdict is more restrictive the override should not take effect
        with patch.object(Issue, "max_verdict", spam.SpamVerdict.BLOCK):
            verdict, reason = s.calculate_verdict(1.0)
            self.assertEqual(spam.SpamVerdict.BLOCK, verdict, "Unexpected verdict")
            self.assertEqual("ml inference score", reason, "Unexpected verdict reason")

        # ALLOW verdicts should not get overriden
        verdict, reason = s.calculate_verdict(0.1)
        self.assertEqual(spam.SpamVerdict.ALLOW, verdict, "Unexpected verdict")
        self.assertEqual("ml inference score", reason, "Unexpected verdict reason")

    def test_timestamps(self):
        now = datetime.now()
        ts = Timestamp()
        ts.FromDatetime(now)
        issue = self.mock_issue
        issue["created_at"] = ts
        pb_issue = spam.Issue(**issue)

        i = Issue(pb_issue, MockContext())
        self.assertEqual(
            now, i.spammable.created_at.ToDatetime(), "Timestamp not set correctly"
        )

        # Set a datetime that is out of range
        pb_issue.created_at.seconds = -1000000000000
        i = Issue(pb_issue, MockContext())
        self.assertFalse(
            i.spammable.HasField("created_at"),
            "Invalid timestamp should be removed from protobuf",
        )
