import unittest

from app import gcs

class TestGCS(unittest.TestCase):
    def test_parse_url(self):
        url = "gs://test-bucket/path/to/object.txt"
        bucket_name, path = gcs.parse_url(url)

        self.assertEqual("test-bucket", bucket_name, "Bucket not parsed correctly")
        self.assertEqual("path/to/object.txt", path, "Path not parsed correctly")
