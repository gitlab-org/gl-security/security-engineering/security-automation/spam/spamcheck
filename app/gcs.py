"""Helper functions for interacting with GCS."""

import json
import os
import time
from pathlib import Path

from google.cloud import storage

from app import logger
from app.helpers import run_async

_WATCH_INTERVAL = 60

log = logger.logger

def parse_url(url: str) -> (str, str):
    """Parse GS url and return bucket and path."""
    url = url.lower().removeprefix("gs://")
    parts = url.split("/")
    bucket = parts[0]
    path = "/".join(parts[1:])
    return (bucket, path)


class Bucket:
    """Helper class for interacting with a GCS bucket"""

    def __init__(self, bucket: str) -> None:
        self.bucket = storage.Client().get_bucket(bucket)

    def read_file(self, filename: str) -> str:
        """Read a file as a string from GCS.

        Args:
            filename (str): The bucket path of the file.
        """
        blob = self.bucket.get_blob(filename)
        return blob.download_as_string()

    def write_json(self, path: str, data: dict) -> None:
        """Convert a dict to JSON and write to GCS.

        Args:
            path (str): The bucket path to write the blob.
            data (dict): The dictionary to save.
        """
        json_str = json.dumps(data)
        blob = self.bucket.blob(path)
        blob.upload_from_string(json_str)


@run_async()
def watch_file(uri: str, download_path: str) -> None:
    """Watch a GCS file and download when changed.

    Args:
        uri (str): The GCS uri to watch (must start gith gs://).
        download_path (dict): The local path to download the file.
    """

    bkt, path = parse_url(uri)
    bucket = Bucket(bkt)
    cached_gcs_stamp = 0
    cached_local_stamp = 0

    download_dir = os.path.dirname(download_path)
    Path(download_dir).mkdir(parents=True, exist_ok=True)

    while True:
        blob = bucket.bucket.blob(path)
        blob.reload()
        gcs_stamp = blob.time_created

        try:
            local_stamp = os.stat(download_path).st_mtime
        except FileNotFoundError:
            local_stamp = 0

        if local_stamp == 0 or gcs_stamp != cached_gcs_stamp or local_stamp != cached_local_stamp:
            log.info("Downloading changed GCS file", extra={"file": uri})
            with open(download_path, "wb") as handle:
                blob.download_to_file(handle)
            cached_gcs_stamp = gcs_stamp
            cached_local_stamp = os.stat(download_path).st_mtime

        time.sleep(_WATCH_INTERVAL)
