"""Spamcheck helper functions"""
import functools
import time
from threading import Thread

from app import logger

log = logger.logger

def run_async(retries=-1):
    """Decorator to run a function asynchronously with retries and error logging."""
    def decorator_run_async(func):

        @functools.wraps(func)
        def threaded(*args, **kwargs):

            def run():
                count = 0
                while retries < 0 or count < retries:
                    try:
                        func(*args, **kwargs)
                        break
                    except Exception as exp: #pylint: disable=broad-except
                        log.error(exp)
                        time.sleep(30)
                        count += 1

            thread = Thread(target=run, daemon=True)
            thread.start()

        return threaded
    return decorator_run_async
