"""
Deny lists are used to store domain names that should be
disallowed or blocked if classified as spam.
"""
import os
import tempfile
import time

from app import gcs, logger
from app.helpers import run_async
from app.config import config

log = logger.logger


class DenyList:
    """Store domain names that should be blocked/disallowed if flagged as spam."""

    _local_sleep_time = 5
    _gcs_sleep_time = 60

    def __init__(self, file_key: str, threshold_key: float) -> None:
        self._filename = ""
        self.default_threshold = 1.0
        self._file_key = file_key
        self._threshold_key = threshold_key
        self._init_config()
        config.register_callback(self._init_config)
        self._cached_stamp = 0
        self.data = {}

    def has_domain(self, domain: str, confidence: float) -> bool:
        """Check if the deny list contains a domain name and meets the confidence threshold.

        Args:
            domain (str): The domain name to check
            confidence (float): The confidence value from the ML model

        Returns:
            bool
        """
        if domain in self.data:
            return confidence >= self.data[domain]

        # Check to see if the domain name matches a wildcard domain

        split = domain.split(".")
        # Subtract 1 from length since a wildcard will stop at a top level domain (i.e *.com)
        for idx in range(len(split) - 1):
            split[idx] = "*"
            wildcard = ".".join(split[idx:])
            if wildcard in self.data:
                return confidence >= self.data[wildcard]

        return False

    @run_async()
    def watch(self) -> None:
        """Load file and monitor for changes."""
        while True:
            if not self._filename:
                time.sleep(self._gcs_sleep_time)
                continue

            if self._filename.startswith("gs://"):
                self._load_gcs_file()
                time.sleep(self._gcs_sleep_time)
            else:
                self._load_local_file()
                time.sleep(self._local_sleep_time)

    def _load_local_file(self) -> None:
        stamp = os.stat(self._filename).st_mtime
        if stamp != self._cached_stamp:
            log.info("Reloading changed file", extra={"file": self._filename})
            self._load_deny_list(self._filename)
            self._cached_stamp = stamp
            log.info(f"Loaded {len(self.data)} items", extra={"file": self._filename})

    def _load_gcs_file(self) -> None:
        bucket_name, path = gcs.parse_url(self._filename)
        bucket = gcs.Bucket(bucket_name)

        blob = bucket.bucket.blob(path)
        blob.reload()
        stamp = blob.time_created

        if stamp != self._cached_stamp:
            log.info("Reloading changed file", extra={"file": self._filename})
            with tempfile.NamedTemporaryFile() as tmp:
                blob.download_to_file(tmp)
                tmp.seek(0)
                self._load_deny_list(tmp.name)
            self._cached_stamp = stamp
            log.info(f"Loaded {len(self.data)} items", extra={"file": self._filename})

    def _load_deny_list(self, filename: str) -> None:
        items = {}

        try:
            with open(filename, encoding="utf-8") as handle:
                for line in handle:
                    parts = line.strip().lower().split(":")
                    if len(parts) > 1:
                        try:
                            items[parts[0]] = float(parts[1])
                        except ValueError:
                            log.error(
                                "Unable to add item to deny list", extra={"item": line}
                            )
                    else:
                        items[parts[0]] = self.default_threshold
        except FileNotFoundError:
            log.error("file not found", extra={"file_name": filename})

        self.data = items

    def _init_config(self) -> None:
        self._filename = config.get_string(self._file_key)
        self.default_threshold = config.get_float(
            self._threshold_key, self.default_threshold
        )
