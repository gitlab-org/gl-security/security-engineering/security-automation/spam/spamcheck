"""Process an snippet to determine if it is spam or not."""

from google.protobuf.json_format import MessageToDict

# The generic classifier defaults to using the issue model since it has the most training data.
from spam_classifiers.issue import classifier

import api.v1.spamcheck_pb2 as spam
from app import logger
from app.config import config
from app.spammable import Spammable
from server.interceptors import SpamCheckContext

log = logger.logger


class Generic(Spammable):
    """Analyze a generic spammable to determine if it is spam."""

    def __init__(self, spammable: spam.Generic, context: SpamCheckContext) -> None:
        super().__init__(spammable, context, classifier)

    def to_dict(self) -> dict:
        """Return the dictionary representation of the spammable."""
        spammable_dict = MessageToDict(
            self._spammable, always_print_fields_with_no_presence=True
        )
        spammable_dict["correlation_id"] = str(self.context.correlation_id)
        spammable_dict["title"] = ""
        spammable_dict["description"] = spammable_dict.pop("text")
        return spammable_dict

    def full_text(self) -> str:
        return self.spammable.text

    def type(self) -> str:
        s_type = self._spammable.type

        if s_type == "":
            s_type = "generic"

        return s_type.lower()


Generic.set_max_verdict()
config.register_callback(Generic.set_max_verdict)
