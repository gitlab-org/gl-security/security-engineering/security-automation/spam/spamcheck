"""Process an issue to determine if it is spam or not."""
from spam_classifiers.issue import classifier

import api.v1.spamcheck_pb2 as spam
from app import logger
from app.config import config
from app.spammable import Spammable
from server.interceptors import SpamCheckContext


log = logger.logger

class Issue(Spammable):
    """Analyze a GitLab issue to determine if it is spam."""

    def __init__(self, issue: spam.Issue, context: SpamCheckContext) -> None:
        super().__init__(issue, context, classifier)

    def full_text(self) -> str:
        return f"{self.spammable.title} {self.spammable.description}"


Issue.set_max_verdict()
config.register_callback(Issue.set_max_verdict)
