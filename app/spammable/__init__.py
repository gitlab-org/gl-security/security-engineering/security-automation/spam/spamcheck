"""Logic to perform spam/ham classification"""

import functools
import re
import sys
from abc import ABC, abstractmethod
from importlib.metadata import version
from typing import Any, List, Set

from google.protobuf.json_format import MessageToDict

from api.v1.spamcheck_pb2 import SpamVerdict
from app import data_store, event, logger, queue
from app.config import config
from app.deny_list import DenyList
from server.interceptors import SpamCheckContext

log = logger.logger


domain_disallow_list = DenyList("domain_disallow_list", "disallow_list_threshold")
domain_block_list = DenyList("domain_block_list", "disallow_list_threshold")

domain_disallow_list.watch()
domain_block_list.watch()


class Spammable(ABC):
    """Base class for spammable types."""

    allow_list = {}
    deny_list = {}
    allowed_domains = {}

    url_re = re.compile(r"https?:\/\/([a-z0-9][-a-z0-9]{0,64}\.[-a-z0-9\.]{1,256})")

    # Currently maximum allowed value is conditional allow to limit false positives.
    max_verdict = SpamVerdict.CONDITIONAL_ALLOW

    _verdict_rankings = {
        SpamVerdict.ALLOW: 1,
        SpamVerdict.CONDITIONAL_ALLOW: 2,
        SpamVerdict.DISALLOW: 3,
        SpamVerdict.BLOCK: 4,
    }

    _verdict_mapping = {
        "ALLOW": SpamVerdict.ALLOW,
        "CONDITIONAL_ALLOW": SpamVerdict.CONDITIONAL_ALLOW,
        "DISALLOW": SpamVerdict.DISALLOW,
        "BLOCK": SpamVerdict.BLOCK,
    }

    def __init__(
        self, spammable: Any, context: SpamCheckContext, classifier: None
    ) -> None:
        self.context = context
        self.spammable = spammable
        self.classifier = classifier

    @abstractmethod
    def full_text(self) -> str:
        """Spammable text. Must override in inheriting classes."""

    @classmethod
    def set_max_verdict(cls):
        """Set the maximum verdict for the spammable class."""
        key = f"{cls.__name__}.max_verdict".lower()
        verdict_value = config.get_string(key).upper()

        try:
            max_verdict = cls._verdict_mapping[verdict_value]
            if max_verdict != cls.max_verdict:
                cls.max_verdict = max_verdict
                log.debug(
                    f"Set max verdict for {cls.__name__}",
                    extra={"verdict": verdict_value},
                )
        except KeyError:
            valid_args = ", ".join(cls._verdict_mapping.keys())
            log.error(f"Max verdict must be in [{valid_args}]. Got: {verdict_value}")

    @classmethod
    def set_filters(cls):
        """Set the filters for based on current config settings."""
        cls.allow_list = config.get("filter.allow_list", {})
        cls.deny_list = config.get("filter.deny_list", {})
        cls.allowed_domains = set(config.get("filter.allowed_domains", []))
        log.debug("Configured filters")

    @property
    def spammable(self) -> Any:
        """spam.Spammable: The spammable to analyze for spam"""
        return self._spammable

    @spammable.setter
    def spammable(self, spammable: Any):
        self._spammable = spammable
        self._validate_timestamps()
        self._email_allowed = self.email_allowed(spammable.user.emails)
        if spammable.project:
            self._project_allowed = self.project_allowed(spammable.project.project_id)
        else:
            self._project_allowed = True

    def verdict(self) -> SpamVerdict:
        """Analyze the spammable and determine if spam.

        Returns:
            SpamVerdict
        """

        # If the project is not allowed then this may be an indication that the model
        # does not generalize well to the spammables in that project. In this case we will
        # circumvent evaluating the spammable.
        if not self._project_allowed:
            return self._verdict(SpamVerdict.NOOP, 0.0, "project not allowed", False)
        if not self.classifier:
            return self._verdict(SpamVerdict.NOOP, 0.0, "classifier not loaded", False)

        spammable_dict = self.to_dict()
        confidence = self.classifier.score(spammable_dict)

        if self._email_allowed:
            verdict, reason = SpamVerdict.ALLOW, "email allowed"
        else:
            verdict, reason = self.calculate_verdict(confidence)

        spammable_dict["verdict"] = SpamVerdict.Verdict.Name(verdict)
        data_store.save(self.type(), spammable_dict, confidence)

        return self._verdict(verdict, confidence, reason, True)

    def calculate_verdict(self, confidence: float) -> (SpamVerdict, str):
        """Convert an ML confidence value to a spam verdict.

        Args:
            confidence (float): The ML confidence value

        Returns:
            SpamVerdict, str
        """
        reason = "ml inference score"
        for threshold, vdict in self._inference_scores().items():
            if confidence >= threshold:
                verdict, max_reason = self._maximum_verdict(vdict, confidence)
                return verdict, f"{reason} {max_reason}".strip()

        return (
            SpamVerdict.NOOP,
            f"unable to determine verdict from inference score: {confidence}",
        )

    def _verdict(
        self, verdict: int, confidence: float, reason: str, evaluated: bool
    ) -> SpamVerdict:
        fields = {
            "correlation_id": str(self.context.correlation_id),
            "metric": "spamcheck_verdicts",
            "spammable_type": self.type(),
            "email_allowlisted": self._email_allowed,
            "project_allowed": self._project_allowed,
            "project_path": self._spammable.project.project_path,
            "project_id": self._spammable.project.project_id,
            "user_name": self._spammable.user.username,
            "user_in_project": self._spammable.user_in_project,
            "verdict": SpamVerdict.Verdict.Name(verdict),
            "reason": reason,
            "confidence": confidence,
            "evaluated": evaluated,
            "classifier_version": version("spam-classifiers"),
        }
        log.info("Verdict calculated", extra=fields)
        if verdict not in (SpamVerdict.ALLOW, SpamVerdict.NOOP):
            evnt = event.Event(event.VERDICT, fields)
            queue.publish(evnt)
        return SpamVerdict(
            verdict=verdict, score=confidence, reason=reason, evaluated=evaluated
        )

    def project_allowed(self, project_id: int) -> bool:
        """Determine if a project should be tested for spam.

        Args:
            project_id (int): The GitLab project ID

        Returns:
            bool
        """
        if len(self.allow_list) != 0:
            if self.allow_list.get(project_id) is not None:
                return True
            return False

        if len(self.deny_list) != 0:
            if self.deny_list.get(project_id) is not None:
                return False
            return True

        return True

    def email_allowed(self, emails: List) -> bool:
        """Determine if a user email should be exempt from spam checking.

        Args:
            emails (list): A list of Emails represented by protobuf objects

        Returns:
            bool
        """
        for email in emails:
            if not "@" in email.email:
                continue
            domain = email.email.split("@")[-1]
            if email.verified and domain in self.allowed_domains:
                return True
        return False

    def _inference_scores(self) -> dict:
        block = self._get_threshold("block")
        disallow = self._get_threshold("disallow")
        conditional_allow = self._get_threshold("conditional_allow")

        return {
            block: SpamVerdict.BLOCK,
            disallow: SpamVerdict.DISALLOW,
            conditional_allow: SpamVerdict.CONDITIONAL_ALLOW,
            0.0: SpamVerdict.ALLOW,
        }

    def _get_threshold(self, kind: str) -> float:
        key = f"{self.type()}.{kind}_threshold"
        generic_key = f"generic.{kind}_threshold"

        if kind == "block":
            default_threshold = config.DEFAULT_BLOCK_THRESHOLD
        elif kind == "disallow":
            default_threshold = config.DEFAULT_DISALLOW_THRESHOLD
        elif kind == "conditional_allow":
            default_threshold = config.DEFAULT_CONDITIONAL_ALLOW_THRESHOLD
        else:
            default_threshold = 0.0

        if config.is_set(key):
            threshold = config.get_float(key)
        else:
            # Favor the generic verdict over the default
            threshold = config.get_float(generic_key, default_threshold)

        return threshold

    def type(self) -> str:
        """Get the string representation of the spammable type."""
        return type(self).__name__.lower()

    def to_dict(self) -> dict:
        """Return the dictionary representation of the spammable."""
        spammable_dict = MessageToDict(
            self._spammable, always_print_fields_with_no_presence=True
        )
        spammable_dict["correlation_id"] = str(self.context.correlation_id)
        return spammable_dict

    def _maximum_verdict(
        self, verdict: SpamVerdict, confidence: float
    ) -> (SpamVerdict, str):
        max_verdict_rank = self._verdict_rankings[self.max_verdict]
        verdict_rank = self._verdict_rankings[verdict]
        reason = ""

        if max_verdict_rank < verdict_rank:
            reason = "overriden by max verdict"
            verdict = self.max_verdict

        deny_list_verdict = self._override_via_deny_list(verdict, confidence)
        if verdict != deny_list_verdict:
            verdict = deny_list_verdict
            if verdict == SpamVerdict.BLOCK:
                reason = "overriden by domain block list"
            else:
                reason = "overriden by domain disallow list"

        return verdict, reason

    def _override_via_deny_list(
        self, verdict: SpamVerdict, confidence: float
    ) -> SpamVerdict:
        # BLOCK is the max verdict so we can short circuit
        if verdict == SpamVerdict.BLOCK:
            return verdict

        disallow_ranking = self._verdict_rankings[SpamVerdict.DISALLOW]
        current_ranking = self._verdict_rankings[verdict]

        for domain in self._hyperlinked_domains():
            # If current verdict is already more restrictive don't check the block list
            if current_ranking < disallow_ranking:
                if domain_disallow_list.has_domain(domain, confidence):
                    verdict = SpamVerdict.DISALLOW
                    current_ranking = self._verdict_rankings[verdict]

            if domain_block_list.has_domain(domain, confidence):
                verdict = SpamVerdict.BLOCK
                break  # verdict can't get any higher

        return verdict

    def _hyperlinked_domains(self) -> Set[str]:
        domains = set()

        for match in self.url_re.finditer(self.full_text().lower()):
            domains.add(match.group(1))

        return domains

    def _validate_timestamps(self) -> None:
        timestamp_attributes = ["created_at", "updated_at", "user.created_at"]

        for attr in timestamp_attributes:
            self._validate_timestamp(attr)

    def _validate_timestamp(self, attr: str) -> None:
        try:
            self._get_spammable_attr(attr).ToDatetime()
        except (OverflowError, ValueError):
            extra = {
                "correlation_id": str(self.context.correlation_id),
                "attribute": attr,
            }
            log.warning("Invalid timestamp", extra=extra)
            self._clear_spammable_attr(attr)

    def _clear_spammable_attr(self, attr: str) -> None:
        """Clear an attribute of a spammable."""
        # split the attribute on .
        #
        # pre is everything before the last match and if it is not and empty string
        # it indicates that we are clearing the field of a nested object.
        #
        # post is the field name of the object to clear.
        pre, _, post = attr.rpartition(".")
        if pre:
            obj = self._get_spammable_attr(pre)
        else:
            obj = self.spammable

        obj.ClearField(post)

    def _get_spammable_attr(self, attr: str) -> Any:
        """Get a nested attribute of a spammable.

        This method will recursively call getattr for each resulting element after the
        attribute is split on "."

        For example, if the attr is "user.created_at" this function will essentially return
        getattr(getattr(self.spammable, 'user'), 'created_at').

        Args:
            attr (string): The attribute to get.

        Returns:
            Any
        """
        return functools.reduce(getattr, [self.spammable] + attr.split("."))


Spammable.set_filters()
config.register_callback(Spammable.set_filters)
