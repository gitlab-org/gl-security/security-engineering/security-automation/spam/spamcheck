"""Process an snippet to determine if it is spam or not."""
from spam_classifiers.snippet import classifier

import api.v1.spamcheck_pb2 as spam
from app import logger
from app.config import config
from app.spammable import Spammable
from server.interceptors import SpamCheckContext


log = logger.logger

class Snippet(Spammable):
    """Analyze a GitLab snippet to determine if it is spam."""

    def __init__(self, snippet: spam.Snippet, context: SpamCheckContext) -> None:
        super().__init__(snippet, context, classifier)

    def full_text(self) -> str:
        return f"{self.spammable.title} {self.spammable.description}"

Snippet.set_max_verdict()
config.register_callback(Snippet.set_max_verdict)
