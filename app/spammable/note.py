"""Process a note to determine if it is spam or not."""
from spam_classifiers.note import classifier

import api.v1.spamcheck_pb2 as spam
from app import logger
from app.config import config
from app.spammable import Spammable
from server.interceptors import SpamCheckContext


log = logger.logger

class Note(Spammable):
    """Analyze a GitLab note to determine if it is spam."""

    def __init__(self, note: spam.Generic, context: SpamCheckContext) -> None:
        super().__init__(note, context, classifier)

    def full_text(self) -> str:
        return self.spammable.text


Note.set_max_verdict()
config.register_callback(Note.set_max_verdict)
