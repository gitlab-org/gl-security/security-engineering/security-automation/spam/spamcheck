"""Spamcheck service configuration."""

import argparse
import os
import time
from typing import Any, Callable

from vyper import Vyper

from app import gcs, logger

log = logger.logger


class Config(Vyper):
    """Class to hold spamcheck configuration options"""

    DEFAULT_BLOCK_THRESHOLD = 0.99
    DEFAULT_DISALLOW_THRESHOLD = 0.9
    DEFAULT_CONDITIONAL_ALLOW_THRESHOLD = 0.5
    DEFAULT_MAX_VERDICT = "CONDITIONAL_ALLOW"
    ENV_PREFIX = "spamcheck"

    SPAMMABLE_TYES = {
        "generic": {
            "max_verdict": "ALLOW",
        },
        "issue": {
            "max_verdict": "CONDITIONAL_ALLOW",
        },
        "note": {
            "max_verdict": "ALLOW",  # allow since this started as a generic
        },
        "snippet": {
            "max_verdict": "CONDITIONAL_ALLOW",
        },
    }

    def __init__(self) -> None:
        self._callbacks = []
        self._config_file_name = ""
        super().__init__()

    def register_callback(self, func: Callable) -> None:
        """Register a callback function to be run on the event of a configuration change."""
        self._callbacks.append(func)

    def get(self, key: str, default: Any = None) -> Any:
        """Get a config value for given config key.

        Args:
            key(str): The config key to retrieve.
            default(str): The default value to return if key is not set.
        """
        if default is not None and not self.is_set(key):
            return default

        return super().get(key)

    def get_string(self, key: str, default: str = None) -> str:
        """Get string for given config key.

        Args:
            key(str): The config key to retrieve.
            default(str): The default value to return if key is not set.
        """
        if default is not None and not self.is_set(key):
            return str(default)

        return super().get_string(key)

    def get_float(self, key: str, default: float = None) -> float:
        """Get string for given config key.

        Args:
            key(str): The config key to retrieve.
            default(str): The default value to return if key is not set.
        """
        if default is not None and not self.is_set(key):
            return float(default)

        return super().get_float(key)

    def read_in_config(self) -> None:
        # There appears to be race conditions between writing/reading
        # the config file So retry on error
        for attempt in range(3):
            try:
                super().read_in_config()
                break
            except FileNotFoundError:
                # Only log error if we are looking for a config file that is non-default
                if self._config_file_name != "":
                    log.error(
                        "Config file not found",
                        extra={"file_name": self._config_file_name},
                    )
                break
            except Exception:  # pylint: disable=broad-exception-caught
                if attempt == 2:
                    log.error(
                        "Failed to read config file",
                        extra={"file_name": self._get_config_file()},
                    )
                    break
                time.sleep(1)

    # pylint: disable=too-many-statements,line-too-long
    def load(self) -> None:
        """Load the configuration options.

        Precedence for config:
            CLI arguments
            Environment variables
            Config file
            Defaults
        """

        # Set CLI config options
        parser = argparse.ArgumentParser(description="GitLab Spamcheck Service")
        parser.add_argument(
            "--env",
            type=str,
            choices=["test", "development", "production"],
            help="Application environment",
        )
        parser.add_argument("--grpc-addr", type=str, help="Application bind address")
        parser.add_argument(
            "--log-level",
            type=str,
            choices=["debug", "info", "warning", "error", "fatal"],
            help="Application log level",
        )
        parser.add_argument(
            "--ml-classifiers", type=str, help="Directory location for ML classifiers"
        )
        parser.add_argument(
            "--gcs-bucket", type=str, help="Bucket to save spammable data for labeling"
        )
        parser.add_argument(
            "--google-pubsub-project", type=str, help="Google PubSub project"
        )
        parser.add_argument(
            "--google-pubsub-topic", type=str, help="Google PubSub topic"
        )
        parser.add_argument("--tls-certificate", type=str, help="TLS certificate path")
        parser.add_argument("--tls-private-key", type=str, help="TLS private key path")
        parser.add_argument("-c", "--config", type=str, help="Path to config file")

        parser.add_argument(
            "--domain-disallow-list",
            type=str,
            help="File path to a list of domain names to trigger a DISALLOW verdict if any appear in identified spam",
        )
        parser.add_argument(
            "--disallow-list-threshold",
            type=float,
            help="ML confidence threshold for domain disallow list",
        )
        parser.add_argument(
            "--domain-block-list",
            type=str,
            help="File path to a list of domain names to trigger a BLOCK verdict if any appear in identified spam",
        )
        parser.add_argument(
            "--block-list-threshold",
            type=float,
            help="ML confidence threshold for domain block list",
        )

        # Set config options that apply to each spammable type
        for spammable, default_config in self.SPAMMABLE_TYES.items():
            parser.add_argument(
                f"--{spammable}.max-verdict",
                type=str,
                help=f"Maximum verdict for {spammable}s",
                metavar=self._metavar(f"{spammable}_max_verdict"),
            )

            max_verdict = default_config.get("max_verdict", self.DEFAULT_MAX_VERDICT)
            self.set_default(f"{spammable}.max_verdict", max_verdict)

            for verdict in ["block", "disallow", "conditional_allow"]:
                parser.add_argument(
                    f"--{spammable}.{verdict.replace('_', '-')}-threshold",
                    type=float,
                    help=f"{verdict.upper()} threshold for {spammable}s",
                    metavar=self._metavar(f"{spammable}_{verdict}_threshold"),
                )
                default = default_config.get(
                    "{verdict}_threshold",
                    getattr(self, f"default_{verdict}_threshold".upper()),
                )
                self.set_default(f"{spammable}.{verdict}_threshold", default)

        # ignore unknown args. These will be present when running unit tests
        args, _ = parser.parse_known_args()
        self.bind_args(args.__dict__)

        # Set config file options
        self.set_config_type("yaml")
        self.set_config_name("config")
        self.add_config_path("./config")

        # Set environment variable options
        self.set_env_prefix(self.ENV_PREFIX)
        self.automatic_env()

        # Set defaults
        self.set_default("env", "development")
        self.set_default("grpc_addr", "0.0.0.0:8001")
        self.set_default("log_level", "info")
        self.set_default("google_pubsub_topic", "spamcheck")
        self.set_default("tls_certificate", "./ssl/cert.pem")
        self.set_default("tls_private_key", "./ssl/key.pem")
        self.set_default("filter.allow_list", {})
        self.set_default("filter.deny_list", {})
        self.set_default("filter.allowed_domains", [])
        self.set_default("disallow_list_threshold", 0.55)
        self.set_default("block_list_threshold", 0.9)

        self._parse_config_file()

        self.watch_config()
        self.on_config_change(self._run_callbacks)

        if os.path.exists(self.get("tls_private_key")) and os.path.exists(
            self.get("tls_certificate")
        ):
            self.set("tls_enabled", True)

    def _set_config_file(self, filename: str):
        config_path = os.path.dirname(filename)
        base_name = os.path.basename(filename)
        config_name = os.path.splitext(base_name)[0]
        self.set_config_name(config_name)
        self.add_config_path(config_path)

    def _parse_config_file(self):
        config_file = self.get("config")

        if not config_file:
            self._set_config_file("./config/config.yml")
        elif config_file.startswith("gs://"):
            self._config_file_name = config_file
            self._parse_gcs_config_file(config_file)
        else:
            self._config_file_name = config_file
            self._set_config_file(config_file)

        self.read_in_config()

    def _parse_gcs_config_file(self, config_file):
        download_path = ".gcs/config.yml"
        gcs.watch_file(config_file, download_path)
        self._set_config_file(download_path)

        # wait up to 5 seconds for file to download
        for _ in range(5):
            if os.path.exists(download_path):
                break
            time.sleep(1)

    def _run_callbacks(self):
        log.info("Config file changed")
        for callback in self._callbacks:
            try:
                callback()
            except Exception:  # pylint: disable=broad-exception-caught
                log.error("failed to run config callback")

    def _metavar(self, var: str) -> str:
        return f"{self.ENV_PREFIX}_{var}".upper()


def set_log_level():
    """Set the log level based on the current configuration"""
    lvl = config.get_string("log_level").upper()
    log.set_level(lvl)


def _log_application_config():
    log.debug("Application Configuration", extra=config.all_settings())


config = Config()
config.load()

# Setting the log level here to avoid circular imports
set_log_level()
_log_application_config()

config.register_callback(set_log_level)
config.register_callback(_log_application_config)
