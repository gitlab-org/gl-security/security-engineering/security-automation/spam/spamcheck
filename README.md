# Spamcheck

## Architecture Diagram

The following diagram gives a high-level overview on how the various components of the anti-spam engine interact with each other:

![](docs/architecture.drawio.png)

The basic spamcheck workflow is as follows. If issue metadata meets certain pre-defined conditions then a spam verdict is determined based on custom business logic. If spam status cannot be determined via issue metadata then ML inference is performed on the issue and the confidence ratio of the classification is used to determine the spam verdict.

![](docs/workflow.drawio.png)

## Development

### Running spamcheck with GDK

1. [Login to the GitLab Container Registry](https://docs.gitlab.com/ee/user/packages/container_registry/authenticate_with_container_registry.html)
    ```bash
       docker login registry.gitlab.com -u <username> -p <token>
    ```
1. Run the service locally with docker
    ```bash
    docker pull registry.gitlab.com/gitlab-org/modelops/anti-abuse/spam/spamcheck
    docker run --rm -p 8001:8001 registry.gitlab.com/gitlab-org/modelops/anti-abuse/spam/spamcheck
    ```
1. Optional: [Configure recaptcha](https://docs.gitlab.com/ee/development/spam_protection_and_captcha/exploratory_testing.html) in GDK
1. Enable Spamcheck in GDK
    1. Go to Admin -> Settings -> Reporting
    1. Under the Spam and Anti-bot Protection section
        1. Check `Enable reCAPTCHA` if testing spamcheck with reCAPTCHA
        1. Check `Enable Spam Check via external API endpoint`
        1. Set URL of the external Spam Check endpoint to `grpc://localhost:8001`
1. Optional: To change the maximum verdict values of spamcheck use the `--max-[TYPE]-verdict` options
    ```bash
    docker run --rm -p 8001:8001 registry.gitlab.com/gitlab-org/modelops/anti-abuse/spam/spamcheck --max-generic-verdict block
    ```

### Development Environment

Clone this repo, install dependencies, and run the service. In order to perform ML inference, ensure a classifier is available in the `./classifiers` directory. See the classifiers and configuration section for details.

```bash
git clone REPOSITORY_PATH
cd spamcheck
make deps
cp config/config.example.yml config/config.yml
# Customize config/config.yml if necessary
make run
```

#### Generating gRPC protobuf files

To build the protobuf files when you've made a change:

```bash
make proto
```

To build the Ruby protobuf files

```bash
make proto_ruby
```

### Local development using JupyterLab

As an alternative, we've created a development environment (using a [Jupyter Docker Stacks](https://jupyter-docker-stacks.readthedocs.io/en/latest/index.html) image) that offers a containerized JupyterLab interface to enable one to code and, run Python files and Jupyter notebooks.

To set this up, follow the instructions [here](https://gitlab.com/gitlab-com/gl-security/engineering-and-research/automation-team/docker/jupyter/-/blob/main/README.md).

### Running in Docker

Build the `spamcheck` docker image.

```bash
docker build -t spamcheck-py .
```

Run the docker image and ensure model files are mounted.

```bash
docker run --rm -p 8001:8001 -v "$(pwd)/classifiers:/spamcheck/classifiers" spamcheck-py
```

## Configuration

Configuration options can be loaded via a `yaml` config file, environment variables, or CLI arguments. Preference for config options are:

1.  CLI arguments
2.  Environment variables
3.  Config file options
4.  Application defaults

|Option|Default Value|Description|
|------|--------------|------------|
|`-c`, `--config`<br/>`SPAMCHECK_CONFIG` | `./config/config.yml` | The path to the spamcheck configuration file. Must be in `yaml` format. File can be local or sourced from GCS if passed in the format `gs://bucket-name/path/to/config.yml`|
|`--block-list-threshold`<br/>`SPAMCHECK_BLOCK_LIST_THRESHOLD ` | `0.9` | The default threshold that ML inference must meet to override a spam verdict if the spammable contains a hyperlink to a domain in the block list. |
|`--disallow-list-threshold`<br/>`SPAMCHECK_DISALLOW_LIST_THRESHOLD ` | `0.55` | The default threshold that ML inference must meet to override a spam verdict if the spammable contains a hyperlink to a domain in the disallow list. |
|`--domain-block-list`<br/>`SPAMCHECK_DOMAIN_BLOCK_LIST ` | ``None`` | Path to a text file containing a list of domain names to block. If ML inference classifies something as spam a `BLOCK` verdict will be returned regardless of the maximum verdict set for that particular spammable. File can be local or sourced from GCS if passed in the format `gs://bucket-name/path/to/blocklist.txt`|
|`--domain-disallow-list`<br/>`SPAMCHECK_DOMAIN_DISALLOW_LIST ` | ``None`` | Path to a text file containing a list of domain names to disallow. If ML inference classifies something as spam a `DISALLOW` verdict will override any lesser verdict regardless of the maximum verdict set for that particular spammable. File can be local or sourced from GCS if passed in the format `gs://bucket-name/path/to/disallowlist.txt`|
|`--env`<br/>`SPAMCHECK_ENV`    | `development`| The environment spamcheck is running in. When running in production gRPC reflection is disabled. |
|`--gcs-bucket`<br/>`SPAMCHECK_GCS_BUCKET ` | ``None`` | The GCS bucket to store unlabeled spam for future labeling and training. |
|`--google-pubsub-project`<br/>`SPAMCHECK_GOOGLE_PUBSUB_PROJECT ` | ``None`` | The GCP project where the spamcheck PubSub topic resides for publishing spam events. |
|`--google-pubsub-topic`<br/>`SPAMCHECK_GOOGLE_PUBSUB_TOPIC ` | ``spamcheck`` | The GCP PubSub topic to publish spam events to. |
|`--grpc-addr`<br/>`SPAMCHECK_GRPC_ADDR ` | `0.0.0.0:8001` | The `HOST:PORT` to bind the spamcheck service to. |
|`--log-level`<br/>`SPAMCHECK_LOG_LEVEL ` | `info` | The application log level. |
|`--tls-certificate`<br/>`SPAMCHECK_TLS_CERTIFICATE ` | `./ssl/cert.pem` | The path to the TLS certificate to use for secure connections to spamcheck service. |
|`--tls-private-key`<br/>`SPAMCHECK_TLS_PRIVATE_KEY ` | `./ssl/key.pem` | The path to the TLS private key to use for secure connections to spamcheck service. |


#### Options Specific to Each Spammable

The `<spammable>` placeholders should be replaced by the spammable type. For example, if you want to configure the maximum verdict for issues then you would user the `--issue.max-verdict` CLI argument or the `SPAMCHECK__ISSUE_MAX_VERDICT` environment variable.

|Option|Default Value|Description|
|------|--------------|------------|
|`--<spammable>.max-verdict`<br/>`SPAMCHECK_<SPAMMABLE>_MAX_VERDICT ` | varies for each spammable | Maximum verdict to return for the spammable type. |
|`--<spammable>.block-threshold`<br/>`SPAMCHECK_MAX_GENERIC_VERDICT ` | `0.99` | Minimum ML inference score that must be met to return a block verdict. |
|`--<spammable>.disallow-threshold`<br/>`SPAMCHECK_MAX_GENERIC_VERDICT ` | `0.9` | Minimum ML inference score that must be met to return a block verdict. |
|`--<spammable>.conditional-allow-threshold`<br/>`SPAMCHECK_MAX_GENERIC_VERDICT ` | `0.5` | Minimum ML inference score that must be met to return a block verdict. |


`config/config.yml` has more configuration options. View the [example file](./config/config.example.yml) for details.

### Domain Deny Lists

A domain disallow list or domain block list can be passed to the spamcheck service in order to override spam verdicts with a `DISALLOW` or `BLOCK` result, respectively. These files should contain a list of domain names and if a hyperlink is detected that matches one of the domains then the verdict will be overridden. In order for the verdict override to take effect a spammable needs to have a hyperlink that is in one of the deny lists and an ML inference score that is greater than or equal to the threshold assigned to that domain. By default, all domains will be assigned a threshold based on the `block_list_threshold` or `disallow_list_threshold` but this can be overridden in the domain files. See the example below for what a domain deny list might look like.

```
foo.com
bar.com:0.3
*.baz.com:0.9
```

Let's assume this example is for the domain deny list. In this example, if a spammable contains a hyperlink to `foo.com` and the ML inference score is greater than `0.55` (the default threshold) then a `DISALLOW` verdict will be returned. If the ML inference score is less than `0.55` then the verdict will not be overridden by the disallow list.

Using the same example, if a spammable contains a hyperlink to `bar.com` and the ML inference score exceeds `0.3` then a `DISALLOW` verdict will be returned.

Finally, the domain deny lists support wildcard syntax so if a spammable contains a hyperlink to `spam.baz.com` or `www.spam.baz.com` and the ML inference score is at least `0.9` then a `DISALLOW` verdict will be returned.

## Linting

Run pylint against spamcheck source code:

```bash
make lint
```

## Testing

Tests are located in the `./tests` directory and mirror the python module layout.

### Test suite

Run the test suite:

```bash
make test
```

### Manually

Start the service via `make run`.

Test the gRPC endpoint with a python gRPC client.

```bash
python client.py
```

Test the gRPC endpoint with [grpcurl](https://github.com/fullstorydev/grpcurl):

```bash
grpcurl -plaintext -d "$(cat examples/checkforspamissue.json)" localhost:8001 spamcheck.SpamcheckService/CheckForSpamIssue
```

By default, `grpcurl` will return an empty object, e.g. `{}`, because Protobufs don't
encode 0-value fields and `SpamVerdict.Verdict` is an enum whose default value is `ALLOW` which is 0.

There is a `/healthz` endpoint used for Kubernetes probes and uptime checks:

```bash
grpcurl -plaintext localhost:8001 spamcheck.SpamcheckService/Healthz
```

## Publishing a new gem version

**Note:** The gem version is maintained in [ruby/spamcheck/version.rb](./ruby/spamcheck/version.rb) and will only be updated when the API or client code changes.

1.  Make sure to update `ruby/spamcheck/version.rb` with the new version number. Keep in mind that `bundle update` uses `--major` by default, so all minor version updates should not break backward or cross-protocol compatibility.

2.  Sign up for a [RubyGems account](RubyGems.org) if you already don't have one. You should also be an owner of the [spamcheck gem](https://rubygems.org/gems/spamcheck)

3.  Run `make gem` locally. This command will install dependencies, build the gem and run tests. If successful, the gem will be output to `spamcheck-<version>.gem`.

4.  Sign up for a [RubyGems account](RubyGems.org) if you already don't have one. You should also be an owner of the [spamcheck gem](https://rubygems.org/gems/spamcheck)

5.  Run `gem push spamcheck-<version>.gem` to push the gem. After a successful push, the new gem version should now be publicly available on [RubyGems.org](https://rubygems.org/gems/spamcheck) and ready to use.
